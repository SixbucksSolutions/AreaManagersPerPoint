#!/usr/bin/python3

import json
import argparse
import logging
import re
import pprint
import shapely.wkt
import shapely.geometry
import bisect
import csv


def main():
  args = _parseArgs()

  parsedPoints = _readPointsKmls(args.point_list_kmls)

  parsedPolygons = _readPolygonJson(args.area_manager_json)

  _findPolygonsForPoints(parsedPoints, parsedPolygons)

  _createSheetCsvForPoints(args.output_csv, parsedPoints)


def _readPointsKmls(pointListKmls):
  allStores = {}
  for currKml in pointListKmls:
    try:
      with open(currKml) as pointListHandle:
        fileKml = pointListHandle.read()
        storesFromFile = _parseKml(fileKml)
        allStores.update(storesFromFile)

    except Exception as e:
      logging.error("Could not read KML file {0}: {1}".format(
        currKml, e))

  return allStores


def _parseKml(fullKml):
  descriptionRegex = re.compile(
    r"<description>.*?Address: (.*?)<br>.*Lat: (.*?)<br>.*Lon: (.*?)<br>.*Corrected Venue PL: (.*?)]].*?</description>" )

  pointInfo = { }
  for match in descriptionRegex.finditer(fullKml):
    address = match.group(1)
    pointInfo[address] = {
      'original': {
        'coordinates':    ( match.group(3), match.group(2) ),
        'shapely_point':  shapely.geometry.Point(float(match.group(3)), float(match.group(2))),
        'permalink':   
          "https://www.waze.com/editor/?env=usa&lon={0}&lat={1}&zoom=5".format(
            match.group(3), match.group(2) ),
        'area_manager_polygons': []
      },
      'corrected': {
        'permalink':      match.group(4)
      }
    }

    # logging.info("Information:\n{0}".format(pprint.pformat(pointInfo)))

  return pointInfo


def _readPolygonJson(areaManagerJson):
  try:
    with open(areaManagerJson) as areaManagerHandle:
      wktPolygons = json.load(areaManagerHandle)
  except Exception as e:
    logging.error("Could not read area manager JSON {0}: {1}".format(areaManagerJson, e))

  # logging.debug("Parsed polygons:\n{0}".format(pprint.pformat(wktPolygons)))

  shapelyPolygons = {}

  for currPolyID in sorted(wktPolygons.keys()):
    shapelyPolygons[currPolyID] = shapely.wkt.loads(wktPolygons[currPolyID])

  return shapelyPolygons


def _findPolygonsForPoints(points, polygons):

  for currPointId in points.keys():
    shapelyPoint = points[currPointId]['original']['shapely_point']
    for currPolygonId in polygons.keys():

      shapelyPolygon = polygons[currPolygonId]
      if shapelyPoint.within(shapelyPolygon):
        # Do an in-order sort to keep them more readable
        bisect.insort(points[currPointId]['original']['area_manager_polygons'], currPolygonId)

def _createSheetCsvForPoints(csvFilename, points):
  with open(csvFilename, 'w') as csvFile:
    sheetWriter = csv.writer(csvFile,quoting=csv.QUOTE_NONNUMERIC)

    # Write header row
    sheetWriter.writerow(
      [ "store_address", "original_lon", "original_lat", "original_lam_polygon", "original_link", "corrected_permalink" ]
    );

    for currAddress in sorted(points.keys()):
      currPoint = points[currAddress]

      # Only output points we found a polygon for
      if len(currPoint['original']['area_manager_polygons']) > 0:
        sheetWriter.writerow(
          [
            currAddress, 
            currPoint['original']['coordinates'][0], 
            currPoint['original']['coordinates'][1],
            currPoint['original']['area_manager_polygons'][0], 
            currPoint['original']['permalink'],
            currPoint['corrected']['permalink'] 
          ]
        )
      


def _parseArgs():
  argParser = argparse.ArgumentParser(
    description="For a list of points, find all area manager polygons each point falls within")
  argParser.add_argument('output_csv', help="CSV file to create for points with polygons")
  argParser.add_argument('area_manager_json', 
    help="JSON file with area manager polygons in Well-Known Text (WKT) format")
  argParser.add_argument('point_list_kmls', help="KML file(s) with point data", nargs="+")
  return argParser.parse_args()


if __name__ == "__main__":
  logging.basicConfig(level=logging.DEBUG)
  main()
